
# AdvancedCarePlans

Basic CRUD application builded for tech test by Szymon Piecek.

## Installation

In main directory use:

```bash
docker-compose up -d
```

## Links

```
http://localhost:5000/ - Swagger
http://localhost/ - AngularApp
```

## Used
* .NET 6
* EFCore
* Fluent Validation
* SQLServer
* Swagger
* Angular2
* Tailwind
* Docker
* Docker-compose
