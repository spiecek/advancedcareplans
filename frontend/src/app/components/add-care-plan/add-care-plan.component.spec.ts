import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCarePlanComponent } from './add-care-plan.component';

describe('AddCarePlanComponent', () => {
  let component: AddCarePlanComponent;
  let fixture: ComponentFixture<AddCarePlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCarePlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCarePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
