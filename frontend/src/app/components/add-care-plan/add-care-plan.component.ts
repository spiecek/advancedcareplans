import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiClientService } from 'src/app/@core/services/api-client.service';

@Component({
  selector: 'app-add-care-plan',
  templateUrl: './add-care-plan.component.html',
  styleUrls: ['./add-care-plan.component.scss'],
})
export class AddCarePlanComponent implements OnInit {
  public completedCheckbox: boolean = false;
  carePlanForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiClientService: ApiClientService
  ) {}

  ngOnInit(): void {
    this.carePlanForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.maxLength(450)]],
      patientName: ['', [Validators.required, Validators.maxLength(450)]],
      userName: ['', [Validators.required, Validators.maxLength(450)]],
      startDate: ['', Validators.required],
      endDate: [''],
      targetDate: ['', Validators.required],
      reason: ['', [Validators.required, Validators.maxLength(1000)]],
      action: ['', [Validators.required, Validators.maxLength(1000)]],
      completed: [false],
      outcome: ['', Validators.maxLength(1000)],
    });
  }

  toggleCompleteCheckbox(value: boolean): void {
    this.completedCheckbox = value;
  }

  onSubmit(): void {
    this.apiClientService.create(this.carePlanForm.value).subscribe(() => {
      this.router.navigate(['/']);
    });
  }
}
