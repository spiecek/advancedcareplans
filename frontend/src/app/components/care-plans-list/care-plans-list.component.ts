import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CarePlaneDto } from 'src/app/@core/models/care-plan-dto';
import { ApiClientService } from 'src/app/@core/services/api-client.service';

@Component({
  selector: 'app-care-plans-list',
  templateUrl: './care-plans-list.component.html',
  styleUrls: ['./care-plans-list.component.scss'],
})
export class CarePlansListComponent implements OnInit {
  carePlans?: CarePlaneDto[];

  constructor(
    private apiClientService: ApiClientService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getAllCarePlans();
  }

  getAllCarePlans(): void {
    this.apiClientService.getAll().subscribe((data) => {
      this.carePlans = data;
    });
  }

  deleteCarePlan(id: string): void {
    this.apiClientService.delete(id).subscribe((res) => {
      this.carePlans = this.carePlans?.filter((item) => item.id !== id);
    });
  }

  addNewRedirect(): void {
    this.router.navigate(['add']);
  }
}
