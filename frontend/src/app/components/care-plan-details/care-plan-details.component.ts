import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CarePlaneDto } from 'src/app/@core/models/care-plan-dto';
import { ApiClientService } from 'src/app/@core/services/api-client.service';

@Component({
  selector: 'app-care-plan-details',
  templateUrl: './care-plan-details.component.html',
  styleUrls: ['./care-plan-details.component.scss'],
})
export class CarePlanDetailsComponent implements OnInit {
  public completedCheckbox: boolean = false;
  carePlanForm: FormGroup;
  id: string;
  carePlan: CarePlaneDto;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiClientService: ApiClientService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.carePlanForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.maxLength(450)]],
      patientName: ['', [Validators.required, Validators.maxLength(450)]],
      userName: ['', [Validators.required, Validators.maxLength(450)]],
      startDate: ['', Validators.required],
      endDate: [''],
      targetDate: ['', Validators.required],
      reason: ['', [Validators.required, Validators.maxLength(1000)]],
      action: ['', [Validators.required, Validators.maxLength(1000)]],
      completed: [false],
      outcome: ['', Validators.maxLength(1000)],
    });

    this.apiClientService.get(this.id).subscribe((data: CarePlaneDto) => {
      this.carePlanForm.patchValue(data);
      this.completedCheckbox = data.completed;
    });
  }

  toggleCompleteCheckbox(value: boolean): void {
    this.completedCheckbox = value;
  }

  onSubmit(): void {
    this.apiClientService
      .update(this.id, this.carePlanForm.value)
      .subscribe(() => {
        this.router.navigate(['/']);
      });
  }
}
