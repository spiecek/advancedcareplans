import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCarePlanComponent } from './components/add-care-plan/add-care-plan.component';
import { CarePlanDetailsComponent } from './components/care-plan-details/care-plan-details.component';
import { CarePlansListComponent } from './components/care-plans-list/care-plans-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'careplans', pathMatch: 'full' },
  { path: 'careplans', component: CarePlansListComponent },
  { path: 'careplans/:id', component: CarePlanDetailsComponent },
  { path: 'add', component: AddCarePlanComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
