import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddCarePlanComponent } from './components/add-care-plan/add-care-plan.component';
import { CarePlanDetailsComponent } from './components/care-plan-details/care-plan-details.component';
import { CarePlansListComponent } from './components/care-plans-list/care-plans-list.component';

@NgModule({
  declarations: [
    AppComponent,
    AddCarePlanComponent,
    CarePlanDetailsComponent,
    CarePlansListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
