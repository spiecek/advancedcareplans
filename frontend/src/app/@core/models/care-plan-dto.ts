export interface CarePlaneDto {
    id: string;
    title: string;
    patientName: string;
    userName: string;
    startDate: Date;
    endDate: Date;
    targetDate: Date;
    reason: string;
    action: string;
    completed: boolean;
    outcome: string;
}