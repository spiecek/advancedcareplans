export interface UpdateCarePlanRequest {
    title: string;
    patientName: string;
    userName: string;
    startDate: string;
    endDate: string | null;
    targetDate: string;
    reason: string;
    action: string;
    completed: boolean;
    outcome: string;
}