import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CarePlaneDto } from '../models/care-plan-dto';
import { CreateCarePlanRequest } from '../models/create-care-plan-request';
import { UpdateCarePlanRequest } from '../models/update-care-plan-request';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class ApiClientService {
  private readonly apiUrl = `${environment.apiUrl}careplan`;

  constructor(private httpClient: HttpClient) {}

  create(data: any): Observable<CarePlaneDto> {
    let startDate = new Date(data.startDate).toISOString();
    let targetDate = new Date(data.targetDate).toISOString();
    let endDate = data.checked ? new Date(data.endDate).toISOString() : null;

    let body: CreateCarePlanRequest = {
      title: data.title,
      patientName: data.patientName,
      userName: data.userName,
      startDate: startDate,
      endDate: endDate,
      targetDate: targetDate,
      reason: data.reason,
      action: data.action,
      completed: data.completed,
      outcome: data.outcome,
    };

    return this.httpClient.post<CarePlaneDto>(
      this.apiUrl,
      JSON.stringify(body),
      httpOptions
    );
  }

  update(id: string, data: any): Observable<CarePlaneDto> {
    let startDate = new Date(data.startDate).toISOString();
    let targetDate = new Date(data.targetDate).toISOString();
    let endDate = data.checked ? new Date(data.endDate).toISOString() : null;

    let body: UpdateCarePlanRequest = {
      title: data.title,
      patientName: data.patientName,
      userName: data.userName,
      startDate: startDate,
      endDate: endDate,
      targetDate: targetDate,
      reason: data.reason,
      action: data.action,
      completed: data.completed,
      outcome: data.outcome,
    };

    console.log(body);

    return this.httpClient.put<CarePlaneDto>(
      this.apiUrl + `/${id}`,
      JSON.stringify(body),
      httpOptions
    );
  }

  get(id: string): Observable<CarePlaneDto> {
    return this.httpClient.get<CarePlaneDto>(this.apiUrl + `/${id}`);
  }

  getAll(): Observable<CarePlaneDto[]> {
    return this.httpClient.get<CarePlaneDto[]>(this.apiUrl + '/getall');
  }

  delete(id: string): Observable<any> {
    return this.httpClient.delete(this.apiUrl + `/${id}`);
  }
}
