﻿using AdvancedCarePlans.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AdvancedCarePlans.Infrastructure.Persistence.Configurations
{
    public class CarePlanConfiguration : IEntityTypeConfiguration<CarePlan>
    {
        public void Configure(EntityTypeBuilder<CarePlan> builder)
        {
            builder.Property(x => x.Title)
                .HasMaxLength(450)
                .IsRequired();

            builder.Property(x => x.PatientName)
                .HasMaxLength(450)
                .IsRequired();

            builder.Property(x => x.UserName)
                .HasMaxLength(450)
                .IsRequired();

            builder.Property(x => x.StartDate)
                .IsRequired();

            builder.Property(x => x.TargetDate)
                .IsRequired();

            builder.Property(x => x.Reason)
                .HasMaxLength(1000)
                .IsRequired();

            builder.Property(x => x.Action)
                .HasMaxLength(1000)
                .IsRequired();

            builder.Property(x => x.Outcome)
                .HasMaxLength(1000);
        }
    }
}