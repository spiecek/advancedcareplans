﻿using AdvancedCarePlans.Application.DTOs.CarePlan;
using AdvancedCarePlans.Application.Exceptions;
using AdvancedCarePlans.Application.Interfaces;
using AdvancedCarePlans.Domain.Entities;
using AdvancedCarePlans.Infrastructure.Persistence;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace AdvancedCarePlans.Infrastructure.Repositories
{
    public class CarePlanRepository : ICarePlanRepository
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IMapper _mapper;

        public CarePlanRepository(ApplicationDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<CarePlanResponse> AddAsync(CreateCarePlanRequest request)
        {
            var carePlan = _mapper.Map<CarePlan>(request);

            await _dbContext.CarePlans.AddAsync(carePlan);
            await _dbContext.SaveChangesAsync();

            var result = _mapper.Map<CarePlanResponse>(carePlan);

            return result;
        }

        public async Task<CarePlanResponse> UpdateAsync(Guid carePlanId, UpdateCarePlanRequest request)
        {
            var carePlan = await _dbContext.CarePlans.FindAsync(carePlanId);
            if (carePlan is null)
            {
                throw new NotFoundException("Cannot found care plan");
            }

            carePlan.Title = request.Title;
            carePlan.PatientName = request.PatientName;
            carePlan.UserName = request.UserName;
            carePlan.StartDate = request.StartDate;
            carePlan.TargetDate = request.TargetDate;
            carePlan.Reason = request.Reason;
            carePlan.Action = request.Action;
            carePlan.Completed = request.Completed;
            carePlan.EndDate = request.EndDate;
            carePlan.Outcome = request.Outcome;

            _dbContext.CarePlans.Update(carePlan);
            await _dbContext.SaveChangesAsync();

            var result = _mapper.Map<CarePlanResponse>(carePlan);

            return result;
        }

        public async Task<CarePlanResponse> GetAsync(Guid carePlanId)
        {
            var carePlan = await _dbContext.CarePlans.FindAsync(carePlanId);
            if (carePlan is null)
            {
                throw new NotFoundException("Cannot found care plan");
            }

            var result = _mapper.Map<CarePlanResponse>(carePlan);

            return result;
        }

        public async Task DeleteAsync(Guid carePlanId)
        {
            var carePlan = await _dbContext.CarePlans.FindAsync(carePlanId);
            if (carePlan is null)
            {
                throw new NotFoundException("Cannot found care plan");
            }

            _dbContext.CarePlans.Remove(carePlan);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<CarePlanResponse>> GetListAsync()
        {
            var result = await _dbContext.CarePlans
                .Select(x => _mapper.Map<CarePlanResponse>(x))
                .ToListAsync();

            return result;
        }
    }
}