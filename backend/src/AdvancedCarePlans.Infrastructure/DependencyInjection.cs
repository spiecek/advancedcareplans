﻿using System.Reflection;
using AdvancedCarePlans.Application.Interfaces;
using AdvancedCarePlans.Infrastructure.Persistence;
using AdvancedCarePlans.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AdvancedCarePlans.Infrastructure
{
    public static class DependencyInjection
    {
        public static void SetupDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("LocalConnection");
            var migrationAssembly = typeof(ApplicationDbContext).GetTypeInfo().Assembly.FullName;

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    connectionString,
                    x => x.MigrationsAssembly(migrationAssembly)));
        }

        public static void SetupServices(this IServiceCollection service)
        {
            service.AddScoped<ICarePlanRepository, CarePlanRepository>();
        }
    }
}