﻿using AdvancedCarePlans.Application;
using AdvancedCarePlans.Infrastructure;

namespace AdvancedCarePlans.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddApplication();

            services.SetupServices();

            services.SetupDatabase(Configuration);

            services.AddAuthorization();

            services.AddSwaggerGen();

            services.AddControllers();

            services.AddRouting(options =>
            {
                options.LowercaseUrls = true;
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CleanArchitecture.WebApi");
            });

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}