﻿using AdvancedCarePlans.Application.DTOs.CarePlan;
using AdvancedCarePlans.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace AdvancedCarePlans.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CarePlanController : ControllerBase
    {
        private readonly ICarePlanRepository _carePlanRepository;

        public CarePlanController(ICarePlanRepository carePlanRepository)
        {
            _carePlanRepository = carePlanRepository;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var result = await _carePlanRepository.GetAsync(id);

            return Ok(result);
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> GetCarePlans()
        {
            var result = await _carePlanRepository.GetListAsync();

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateCarePlanRequest request)
        {
            var result = await _carePlanRepository.AddAsync(request);

            return Ok(result);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, UpdateCarePlanRequest request)
        {
            var result = await _carePlanRepository.UpdateAsync(id, request);

            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _carePlanRepository.DeleteAsync(id);

            return Ok();
        }
    }
}