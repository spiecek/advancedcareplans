﻿using System.Reflection;
using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;

namespace AdvancedCarePlans.Application
{
    public static class DependencyInjection
    {
        public static void AddApplication(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddFluentValidation(x => { x.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()); });
        }
    }
}