﻿using AdvancedCarePlans.Application.DTOs.CarePlan;
using AdvancedCarePlans.Domain.Entities;
using AutoMapper;

namespace AdvancedCarePlans.Application.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<CreateCarePlanRequest, CarePlan>();
            CreateMap<CarePlan, CarePlanResponse>();
        }
    }
}