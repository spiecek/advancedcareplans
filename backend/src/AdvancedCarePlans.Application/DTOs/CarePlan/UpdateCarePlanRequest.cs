﻿namespace AdvancedCarePlans.Application.DTOs.CarePlan
{
    public class UpdateCarePlanRequest
    {
        public string Title { get; set; }

        public string PatientName { get; set; }

        public string UserName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime TargetDate { get; set; }

        public string Reason { get; set; }

        public string Action { get; set; }

        public bool Completed { get; set; }

        public string Outcome { get; set; }
    }
}