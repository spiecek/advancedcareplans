﻿using AdvancedCarePlans.Application.DTOs.CarePlan;
using FluentValidation;

namespace AdvancedCarePlans.Application.Validators.CarePlan
{
    public class CreateCarePlanRequestValidator : AbstractValidator<CreateCarePlanRequest>
    {
        public CreateCarePlanRequestValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("Title required.")
                .MaximumLength(450).WithMessage("Maximum length for title is 450.");

            RuleFor(x => x.PatientName)
                .NotEmpty().WithMessage("Patient name required.")
                .MaximumLength(450).WithMessage("Maximum length for patient name is 450.");

            RuleFor(x => x.UserName)
                .NotEmpty().WithMessage("Username required.")
                .MaximumLength(450).WithMessage("Maximum length for username is 450.");

            RuleFor(x => x.StartDate)
                .NotEmpty().WithMessage("Start date required.");

            RuleFor(x => x.TargetDate)
                .NotEmpty().WithMessage("Target date required.");

            RuleFor(x => x.Reason)
                .NotEmpty().WithMessage("Reason required.")
                .MaximumLength(1000).WithMessage("Maximum length for reason is 1000.");

            RuleFor(x => x.Action)
                .NotEmpty().WithMessage("Action required.")
                .MaximumLength(1000).WithMessage("Maximum length for action is 1000.");

            RuleFor(x => x.EndDate)
                .GreaterThan(x => x.StartDate).WithMessage("End date must be greater than end date.")
                .NotEmpty().WithMessage("End date is required when care plan is completed.")
                .When(x => x.Completed);

            RuleFor(x => x.Outcome)
                .NotEmpty().WithMessage("Outcome is required when care plan is completed.")
                .When(x => x.Completed);
        }
    }
}