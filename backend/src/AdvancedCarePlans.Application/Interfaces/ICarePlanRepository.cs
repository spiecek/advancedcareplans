﻿using AdvancedCarePlans.Application.DTOs.CarePlan;

namespace AdvancedCarePlans.Application.Interfaces
{
    public interface ICarePlanRepository
    {
        Task<CarePlanResponse> AddAsync(CreateCarePlanRequest request);
        Task<CarePlanResponse> UpdateAsync(Guid carePlanId, UpdateCarePlanRequest request);
        Task<CarePlanResponse> GetAsync(Guid carePlanId);
        Task DeleteAsync(Guid carePlanId);
        Task<List<CarePlanResponse>> GetListAsync();
    }
}