﻿using System.ComponentModel.DataAnnotations;

namespace AdvancedCarePlans.Domain.Entities
{
    public class CarePlan
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(450)]
        public string Title { get; set; }

        [MaxLength(450)]
        public string PatientName { get; set; }

        [MaxLength(450)]
        public string UserName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime TargetDate { get; set; }

        [MaxLength(1000)]
        public string Reason { get; set; }

        [MaxLength(1000)]
        public string Action { get; set; }

        public bool? Completed { get; set; }

        [MaxLength(1000)]
        public string? Outcome { get; set; }
    }
}